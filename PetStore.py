import uuid
import MySQL
from Zwierze import Zwierze

#lista_zwierzat = []
balance = [1000]
def add_pet():
    id = str(uuid.uuid4())
    Gatunek = input("Podaj gatunek: ")
    Kolor = input("Podaj kolor: ")
    Cena = float(input("Podaj cene: "))
    zwierze = Zwierze(id, Gatunek, Kolor, Cena)
    print("Dodano nowe zwierze:\n" + str(zwierze) + "\n")
    
    MySQL.create_pet(Gatunek, Kolor, Cena)
    menu()

def list_pets():
    print("\n============LISTA ZWIERZAT============")
    print(MySQL.pet_list())
    print("========================================\n")
    menu()

def sell_pet():
    global balance

    pet_list = MySQL.pet_list()
    print(pet_list)
    pet_id = input("Podaj Id Zwierzaka")
    print("Wybrano zwierzaka:" + str(pet_id))
    find_pet = MySQL.find_pet(pet_id)
    cena_pet = MySQL.cena_pet(pet_id)
    #saldo = MySQL.balance()
    print(cena_pet)

    if not len(pet_list) > 0:
        print("Podano nieprawidlowe ID")
        menu()


    pet_id == find_pet
    #Saldo = (saldo + cena_pet)
    MySQL.sell_pets(pet_id)
    print("Sprzedano zwierzaka\n" + str(pet_id))
    print("Twoje saldo wynosi:" + str(Saldo))
    print(MySQL.update_balance())
    menu()

def show_balance():

    print("Twoje Saldo to: " + str(MySQL.balance()))
    menu()

def search_pet_by_uuid():
    id = input("Podaj uuid").strip()
    list_of_pets_with_specified_uuid = []
    for zwierze in lista_zwierzat:
        if (zwierze.id == id):
            list_of_pets_with_specified_uuid.append(zwierze)
    for zwierze in list_of_pets_with_specified_uuid:
        print("\n====List zwierząt z konkretnym uuid=====\n")
        print(zwierze)
        print("===========================================\n")
    menu()

def search_pet_by_spiece():
    gatunek = input("Podaj gatunek").strip()
    list_of_pets_with_specified_spiece = []
    for zwierze in lista_zwierzat:
        if (zwierze.gatunek == gatunek):
            list_of_pets_with_specified_spiece.append(zwierze)
    print("\n====List zwierząt z konkretnym gatunkiem=====\n")
    for zwierze in list_of_pets_with_specified_spiece:
        print(zwierze)
    print("===============================================\n")
def search_pet():
    print("""
==================Wyszukiwarka===================
1. Wyszukaj po Uuid
2. Wyszukaj po Gatunku
3. Wyszukaj po Kolorze
4. Wyszukaj po Cenie mniejszej niż
5. Wyszukiwanie po wszystkim    
    """)
    choise = int(input())
    if (choise == 1):
        search_pet_by_uuid()
    if (choise == 2):
        search_pet_by_spiece()


def menu():
    print("""
=================MENU===========================
1. Dodaj zwierzaka
2. Sprzedaj zwierzaka
3. Lista zwierzaków na sprzedaż
4. Wyszukiwarka
5. Saldo
================================================  
    """)
    choise = int(input())
    if (choise == 1):
        add_pet()
    if (choise == 2):
        sell_pet()
    if (choise == 3):
        list_pets()
    if (choise == 4):
        search_pet()
    if (choise == 5):
        show_balance()
menu()