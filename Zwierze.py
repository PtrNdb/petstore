class Zwierze:

    def __init__(self, id, gatunek, kolor, cena):
        self.id = id
        self.gatunek = gatunek
        self.kolor = kolor
        self.cena = cena

    def __str__(self):
        return "id zwierzka: " + self.id + \
               "\ngatunek: " + self.gatunek + \
               "\nkolor: " + self.kolor + \
               "\ncena: " + str(self.cena)

